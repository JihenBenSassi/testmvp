package com.example.mvpapp1.main.Model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "quotesapp";
    private static final int DATABASE_VERSION = 1;
    // table name
    private static final String TABLE_QUOTE= "quotes";
    // Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_QUOTE = "quote";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_QUOTE_TABLE = "CREATE TABLE " + TABLE_QUOTE + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_QUOTE + " TEXT" +")";
        db.execSQL(CREATE_QUOTE_TABLE);

        Log.d("SQLite", "Database table created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if exists
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUOTE);

        // Create tables again
        onCreate(db);
    }
}