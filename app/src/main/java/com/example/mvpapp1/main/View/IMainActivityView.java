package com.example.mvpapp1.main.View;

import com.example.mvpapp1.main.Model.Quote;

import java.util.List;

public interface IMainActivityView {
    void setDataToListView(List<String> quotes);
}
