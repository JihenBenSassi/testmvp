package com.example.mvpapp1.main.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.mvpapp1.main.View.IMainActivityView;

import java.util.ArrayList;
import java.util.List;

public class Quote implements IQuote{
    private SQLiteDatabase  database;
    private SQLiteHelper sqLiteHelper;

    public Quote(IMainActivityView iMainActivityView) {
        database = new  SQLiteHelper((Context) iMainActivityView).getWritableDatabase();
    }

    @Override
    public List<String> getListFromDatabase() {
        List<String> list = new ArrayList<>();
        String sqlQueryText = "SELECT quote FROM quotes";
        Cursor cursor = this.database.rawQuery(sqlQueryText, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    @Override
    public void addQuote(String quote, DBHandler handler) {
        ContentValues values = new ContentValues();
        values.put("quote", quote);
        //add columns later maybe
        // Inserting Row
        try {
            database.insert("quotes", null, values);
            handler.onSuccess();
        } catch (SQLException e){
            handler.onFailure(e);
        }
    }

    @Override
    public void initDB(Context context) {
        sqLiteHelper = new SQLiteHelper(context);
        sqLiteHelper.onUpgrade(database, 1 , 2);
    }
}
