package com.example.mvpapp1.main.Model;

public interface DBHandler {
    void onSuccess();
    void onFailure(Exception e);
}
