package com.example.mvpapp1.main.Model;

import android.content.Context;

import java.util.List;

public interface IQuote {
    List<String> getListFromDatabase();
    void addQuote(String quote, DBHandler handler);
    void initDB(Context context);
}
