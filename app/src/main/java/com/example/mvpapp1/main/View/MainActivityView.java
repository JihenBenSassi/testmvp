package com.example.mvpapp1.main.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mvpapp1.R;
import com.example.mvpapp1.main.Model.Quote;
import com.example.mvpapp1.main.Presenter.IMainActivityPresenter;
import com.example.mvpapp1.main.Presenter.MainActivityPresenter;
import com.example.mvpapp1.main.Model.SQLiteHelper;

import java.util.List;

public class MainActivityView extends AppCompatActivity implements IMainActivityView {

    //reference to the presenter

    private IMainActivityPresenter iPresenter;
    private ListView listView;

    EditText quoteTxt;
    String quote;

    SQLiteHelper myDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iPresenter = new MainActivityPresenter(this);
        iPresenter.initDB(getApplicationContext());
        quoteTxt = (EditText) findViewById(R.id.quote);
        Button btn = (Button)  findViewById(R.id.add);
        listView = (ListView) findViewById(R.id.listView);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Clicked", "yes");
                quote = quoteTxt.getText().toString();
                if(quote.matches("")) {
                    Log.d("empty", "yes");
                    Toast.makeText(getApplicationContext(), "Enter your quote",Toast.LENGTH_LONG).show();
                }
                else {
                    Log.d("quote : ", quote);
                    iPresenter.addQuotes(quote);
                    iPresenter.setDataToListView();
                }
            }
        });
    }

    @Override
    public void setDataToListView(List<String> quotes) {
        myDB = new SQLiteHelper(this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, quotes );
        listView.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
    }
}
