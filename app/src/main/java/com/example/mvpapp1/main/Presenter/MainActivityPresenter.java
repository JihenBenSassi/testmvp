package com.example.mvpapp1.main.Presenter;


import android.content.Context;
import android.util.Log;

import com.example.mvpapp1.main.Model.DBHandler;
import com.example.mvpapp1.main.Model.IQuote;
import com.example.mvpapp1.main.Model.Quote;
import com.example.mvpapp1.main.View.IMainActivityView;

import java.util.List;

public class MainActivityPresenter implements IMainActivityPresenter, DBHandler {

    private IQuote iQuote;
    private IMainActivityView iMainActivityView; // the view

    public MainActivityPresenter(IMainActivityView iMainActivityView) {
        this.iMainActivityView = iMainActivityView;
        iQuote = (IQuote) new Quote(iMainActivityView);
    }


    @Override
    public void addQuotes(String quote) {
       iQuote.addQuote(quote, this);
    }

    @Override
    public void setDataToListView() {
        List<String> list = iQuote.getListFromDatabase();
        iMainActivityView.setDataToListView(list);
    }

    @Override
    public void initDB(Context context) {
        iQuote.initDB(context);
    }

    @Override
    public void onSuccess() {
        setDataToListView();
    }

    @Override
    public void onFailure(Exception e) {
        Log.d("Error",e.toString());
    }
}
