package com.example.mvpapp1.main.Presenter;

import android.content.Context;

public interface IMainActivityPresenter {
    void addQuotes(String quote);
    void setDataToListView();
    void initDB(Context context);
}
